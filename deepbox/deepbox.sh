#!/bin/bash

IMAGE_NAME=ootb/deepbox
NOTEBOOK_PORT=18088

docker run -it --rm \
		--gpus 0 \
		-u `id -u`:`id -g` \
		-p ${NOTEBOOK_PORT}:18088 \
        -v ${PWD}:/host \
		${IMAGE_NAME} \
		/bin/bash
