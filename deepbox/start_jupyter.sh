#!/usr/bin/env bash

/usr/local/bin/jupyter lab \
    --no-browser \
    --ip=0.0.0.0 \
    --port=18088 \
    --allow-root \
    --notebook-dir=/work
